# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Field, Item

GROUP = 10
COMMON = 0

_typeNames = {
    GROUP: 'group',
    COMMON: 'common',
    'group': GROUP,
    'common': COMMON,
}


def get_names(code):
    if code in _typeNames.keys():
        return _typeNames[code]
    else:
        return None


class TopicItem(Item):
    topic = Field()
    reply = Field()
    groupName = Field()
    topicUrl = Field()


class GroupItem(Item):
    groupName = Field()
    groupURL = Field()
    relativeGroups = Field()


class CommonItem(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    html_body = Field()
    url = Field()
    update_time = Field()
    item_type = Field()


