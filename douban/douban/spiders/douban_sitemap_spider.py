# -*- coding: utf-8 -*-
from scrapy.spiders import SitemapSpider
from scrapy.selector import Selector
from scrapy.http import Request
from scrapy.linkextractors import LinkExtractor

from douban import items
from douban.util.select_util import list_first_item
import datetime


# logging configure
import logging
from scrapy.utils.log import configure_logging

configure_logging(install_root_handler=False)
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='douban.log',
                    filemode='w')


class DoubanSitemapSpider(SitemapSpider):
    name = 'DoubanSitemap'
    sitemap_urls = ['http://www.douban.com/sitemap_index.xml']
    sitemap_rules = [
        # ('/group/topic/', 'parse_group_topic'),
        ('/*', 'parse_common')
    ]
    # sitemap_follow = ['/group']  # By default, all sitemaps are followed

    def parse_group_topic(self, response):
        pass # ... scrape group topic

    def parse_common(self, response):
        '''
        save html pages
        :param response:
        :return:
        '''
        item = items.CommonItem()
        item['html_body'] = response.body
        item['url'] = response.url
        item['update_time'] = datetime.datetime.now()
        item['item_type'] = items.COMMON

        return item
