#!/usr/bin/python
# -*- coding:utf-8 -*-

"""
    This file is for initiate mongodb situation

    database: douban_sitemap
    [
    collections: group_topic
    fields:
        group_topic:
            'html': string
            'group_name': string
            'topic_url': string
            'update_time': datetime

    index:
        topic_url
    ]
    So what this do is to delete group_topic if it has existed,and create index for it.
"""

import types
from pymongo import MongoClient
from pymongo import ASCENDING, DESCENDING

DATABASE_NAME = "douban_sitemap"
client = None
DATABASE_HOST = 'localhost'  # "192.168.1.11"
DATABASE_PORT = 27017  # default port
INDEX = {\
    # collection
    'group':\
        {\
            'group_name': {'name': 'group_name'},
        },\
    'common':\
        {
            'url': {'name': 'url', 'unique': True},
        }\
}


def drop_database(name_or_database):
    """
    delete database if it has existed
    """
    if name_or_database and client:
        client.drop_database(name_or_database)


def create_index():
    """
        create index for douban_group.group_topic
    """
    for k, v in INDEX.items():
        for key, kwargs in v.items():
            client[DATABASE_NAME][k].ensure_index(list(key) if type(key) == types.TupleType else key, **kwargs)


if __name__ == "__main__":
    client = MongoClient(DATABASE_HOST, DATABASE_PORT)
    drop_database(DATABASE_NAME)
    create_index()
