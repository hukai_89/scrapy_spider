#! /usr/bin/Python
# -*- coding:utf-8 -*-

from pymongo import MongoClient
import traceback
import datetime


class MongodbPipeline(object):
    """
    save data to mongodb
    connection settings
    """
    MONGODB_SERVER = "localhost"
    MONGODB_PORT = 27017
    MONGODB_DB = "douban_group"

    def __init__(self):
        """
        The only async framework that PyMongo fully supports is Gevent.
        PyMongo provides built-in connection pooling, so some of the benefits of those frameworks can be achieved just by writing multi-threaded code that shares a MongoClient.
        """

        # self.style = color.color_style()
        try:
            self.client = MongoClient(self.MONGODB_SERVER, self.MONGODB_PORT)
            self.db = self.client[self.MONGODB_DB]
        except Exception as e:
            print "ERROR(MongodbPipeline): %s" % (str(e),)
            # print self.style.ERROR("ERROR(MongodbPipeline): %s" % (str(e),))
            traceback.print_exc()

    @classmethod
    def from_crawler(cls, crawler):
        cls.MONGODB_SERVER = crawler.settings.get('SingleMONGODB_SERVER', 'localhost')
        cls.MONGODB_PORT = crawler.settings.getint('SingleMONGODB_PORT', 27017)
        cls.MONGODB_DB = crawler.settings.get('SingleMONGODB_DB', 'douban_group')
        pipe = cls()
        pipe.crawler = crawler
        return pipe

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        topic = {
            'topic': item.get('topic', ''),
            'reply': item.get('reply', ''),
            'group_name': item.get('groupName', ''),
            'topic_url': item.get('topicUrl', ''),
            'update_time': datetime.datetime.now(),
        }
        collection = self.db['group_topic']

        if item.get('topicUrl', None) and not collection.find_one({'topic_url': item.get('topicUrl')}):
            result = collection.insert(topic)

        return item
