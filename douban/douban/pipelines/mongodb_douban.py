#! /usr/bin/Python
# -*- coding:utf-8 -*-

__author__ = 'hukai'
from pymongo import MongoClient
import traceback
import time
from scrapy import log
from douban import items


class MongodbDoubanSitemapPipeline(object):
    def __init__(self, mongo_uri, mongo_db, mongo_port):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        self.mongo_port = mongo_port

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI', 'localhost'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'local'),
            mongo_port=crawler.settings.get('MONGO_PORT', 27017)
        )

    def open_spider(self, spider):
        self.client = MongoClient(self.mongo_uri, self.mongo_port)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        item_type = item.get('item_type', '')
        collection_name = items.get_names(item_type)
        if not collection_name:
            log.msg('there is no collection name', log.DEBUG)
            return item

        if item_type == items.COMMON:
            col = self.db[collection_name]
            if not col.find_one(item.get('url')):
                result = col.insert(dict(item))
            # Todo if update
        elif item_type == items.GROUP:
            # Todo
            pass
        else:
            log.msg('need to add handler', log.DEBUG)
        return item
