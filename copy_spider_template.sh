#! /bin/bash
if [ $# -gt 0 ]; then
    echo "create spider "$1", please configure settings.py when finished"
else
    echo "Usage: "$0" projectname"
    exit
fi
##copy douban spider
#scrapy startproject $project
cp -R douban/ $1
cd $1/
rm -rf *.log SCHEDULER_DIR*
mv douban/ $1
cd $1/
rm -rf *.pyc

##replace some settings
mv settings.example settings.py
#sed -i.bak 's/douban/'$1'/g' settings.py
#sed -i.bak 's/Douban/'$1'/g' settings.py
##remove setttings.py.bak if not necessary
#rm setttings.py.bak
